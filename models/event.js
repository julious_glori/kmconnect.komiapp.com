var mongoose  = require('mongoose');
    bcrypt    = require('bcrypt');
    Schema    = mongoose.Schema;


var EventSchema = new Schema({
  name:        { type: String, required: true  },
  description: { type: String, required: true },
  thumbnail:   String,
  floorplan: [ 
    { _id: { type: Number, required: true },
      label: String,
      image: { type: String, required: true }
    }    
  ],
  tags: {
    show  : [{ 
      _id: { type: Number, required: true },
      label: String
    }],
    interest : [{ 
      _id: { type: Number, required: true },
      label: String
    }],
    trend : [{ 
      _id: { type: Number, required: true },
      label: String
    }]
  }
});

//mongoose model

module.exports = mongoose.model('Event', EventSchema);
