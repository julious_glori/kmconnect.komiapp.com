var mongoose  = require('mongoose');
    bcrypt    = require('bcrypt');
    Schema    = mongoose.Schema;


var BoothSchema = new Schema({
  event_id:   {  type: Schema.Types.ObjectId , ref: 'Event' , required: true},
  name:        { type: String, required: true  },
  description: { type: String, required: true },
  thumbnail:    String ,
  image: String ,
  address: String,
  email: String,
  phone: String,
  url: String,
  title: String,
  coordinates: {
    floorplan_id: String, 
    x: Number ,
    y: Number 
  }
});



//mongoose model

module.exports = mongoose.model('Booth', BoothSchema);
