var mongoose  = require('mongoose');
    bcrypt    = require('bcrypt');
    Schema    = mongoose.Schema;


var AuditSchema = new Schema({
  username:     { type: String, unique: true, required: true },
  deviceToken:  { type: String },
  login:        { type: Date, default: Date.now }  
});

//mongoose model
module.exports = mongoose.model('Audit', AuditSchema);
