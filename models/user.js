var mongoose  = require('mongoose');
    bcrypt    = require('bcrypt');
    Schema    = mongoose.Schema;


var AttendeeSchema = new Schema({
  username:   { type: String, unique: true, required: true  },
  password:   { type: String },
  email:      { type: String, required: true, unique: true} ,
  first_name: String,
  last_name: String,
  job_title: String,
  company: String,
  country: String,
  phone: String,
  avatar: String,
  date_created: { type: Date, default: Date.now },
  event_registration_data : {},
/*
  event_registration_data: [
    {
      event_id : String,
      enable_networking: Boolean,
      tags : {
        interest : [{ _id : Number, label: String }],
        show:  [{ _id : Number, label: String }],
        trend:  [{ _id : Number, label: String }],
      },
      recommended_booths : [  {  type: Schema.Types.ObjectId , ref: 'Event' , required: true} ]
    }

  ],*/
  sessionid:  String


});


AttendeeSchema.pre('save', function(next){
  var attendee = this;
  if(this.isModified('password') || this.isNew){
    if(!attendee.password || attendee.password == '') next();
    bcrypt.genSalt(12, function(err, salt){
      if(err) {
        return next(err);
      }else{
        bcrypt.hash(attendee.password, salt, function(err,hash){
          if(err) return next(err);

          attendee.password = hash;
          next();
        });
      }
    });
  } else {
    return next();
  }
}); 


AttendeeSchema.methods.comparePassword = function(pwd, done){
  bcrypt.compare(pwd, this.password, function(err,isMatch){

    console.log('\n\n\nAttendeeSchema.methods.comparePassword - password: '. pwd);

    if(err) return done(err);
    else done(null, isMatch);

  });
}

//mongoose model

module.exports = mongoose.model('Attendee', AttendeeSchema);
