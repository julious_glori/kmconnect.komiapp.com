$(document).ready(function(){

  // .tabBox
  //   h3
  //   functionArgs

  $('.tabBox').each(function(){

    /* 
    *   1. Create the tab nav 
    */

    //Tab Nav Bar
    var tabs = $('<div>');
    $(tabs).addClass('tabs')

    //For each h3 section add a div.tab in the tab nav 
    $(this).find('h3').each(function(){
      var tabLbl = $('<div>').addClass('tab').append('<span>'+$(this).find('a').attr('alt') +'</span>');
      
      if( $(this).has('sup').length)  $(tabLbl).append('<sup>*</sup>');
      
      $(tabs).append( tabLbl );      
    });

    //mark the first tab as .first
    $(tabs).find('.tab').first().addClass('active').addClass('first');

    //add Tab Nav to tabBox
    $(this).prepend( tabs ) ;


    /* 
    *   2. Set the initial behavior of the tabs and elements
    */

    $(this).find('.tabContent').hide().first().show();


    /* 
    *   3. Tab action handlers
    */    

    //onclick
      //reset - hide everything, remove .active class
      //add .active to this, open this
    $('.tab').click(function(){
      var pos = $(this).index();
      // remove all .active
      $(this).parent('.tabs').find('.tab').removeClass('active');   
      // hide all tabContents
      $(this).parents('.tabBox').find('.tabContent').hide();         

      //mark this as active
      $(this).addClass('active');  
      //show tabBox
      $(this).parents('.tabBox').find('.tabContent ').eq(pos).show();


    });


  });






});