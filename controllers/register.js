var express   = require('express');
var app       = express();

var _err       = require('../helpers/error.js');  

var mongoose  = require('mongoose');
var Attendee  = require('../models/user.js');



//Create single attendee
app.post('/attendee/create', function(req, res) { 

  // console.log('create - req: ',req.body);

  if( !req.body.username || !req.body.password){
    res.status(400).send(_err.format(400, "Missing required fields.", {'req.body': req.body}));    

  }else{  // process and create attendee

    //new Attendee's data
    var newAttendee = new Attendee();

    for( key in req.body ){
      newAttendee[key] = String(req.body[key]);
    }

    //save the attendee
    newAttendee.save(function(err, data){
      if(err){
        /*console.error("err",err);*/
        res.status(409).send(_err.format(409, "Error while saving: ", { 'err': err, err_data: data, req_data: req.body   }));
      }else{          

        //hack: remove password. " select: false " in the schema is not working
        data = data.toObject();
        delete data.password; 

        res.status(200).send(data);          
      } 
    });
  }

});





//Return app
module.exports = app;



// TODO:

/*

    - Move functions out
    - Move models out

    all
      - comments
      - prevent injection
        // http://blog.websecurify.com/2014/08/hacking-nodejs-and-mongodb.html
      - error should not output password
      - bcrypt can slow down create, update, delete of user accounts. Restrict it's use to absolute minimum.

    /create
        //todo: check for valid email & password
        //todo: check if attendee already exists in db
        //todo: password hashing



*/



