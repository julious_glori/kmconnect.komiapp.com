var express  = require('express');
var app      = express();
var _err     = require('../helpers/error.js');
var mongoose = require('mongoose');
var Booth    = require('../models/booth.js');


// route middleware - remove token
// so there's no need to keep dealing with it below.
app.use(function (req, res, next) {
  delete req.body.token;
  next();
});


//Create single booth
app.post('/booth/create', function(req, res) { 

  //new Booth's data
  var newBooth = new Booth();

  for( key in req.body ){
    
    if( typeof(req.body[key]) == 'object'){
      for(var k in req.body[key]){
        newBooth[key][k] = req.body[key][k];
      }
    }else if(key != 'event_id'){
       console.log( key, typeof(req.body[key])  );
      newBooth[key] = String(req.body[key]);
    }else{
      console.log( key, typeof(req.body[key])  );
      newBooth[key] = new mongoose.Types.ObjectId( String(req.body[key] )  ); // cast to ObjectId
    }
  }



  //save the booth
  newBooth.save(function(err, newBooth){
    if(err){
      res.status(409).send(_err.format(409, "Error while saving: ", {'err': err, 'newBooth': newBooth, req_body: req.body   }));
    }else{          
      res.status(200).send(newBooth);          
    } 
  });
});

//Return all booths
app.post('/booths', function(req, res) {
  Booth.find({}, function(err, booths){    
    if(err)                   res.status(409).send( _err.format(409, "Database Lookup Error.", {'err': err}) );      
    else if(booths == null )  res.status(404).send( _err.format(404, "No results found.", {'booths': booths}) )
    else                      res.status(200).send( booths );         
  });
});

//Return booth details
app.post('/booth', function(req, res) {

  if( !req.body['_id']){ //error 400
    res.status(400).send(_err.format(400, "_id Missing.", {'req.body': req.body}));    

  }else{

    var lookup = { _id : String(req.body['_id']) }
    Booth.findOne( lookup , function(err, booth){
      
      if(err) //db Error - 400 
        res.status(409).send( _err.format(409, "Database Lookup Error.", {'err': err}) );      
      else if(booth == null ) //Not found - 404
        res.status(404).send( _err.format(404, "No results found.", {'booth': booth}) ); 
      else
        res.status(200).send( booth );         
    });
  }
});


//Update single booth
app.post('/booth/update', function(req, res) { 

  if( !req.body['_id'] ){
    res.status(400).send(_err.format(400, "_id missing.", {'req.body': req.body}));    

  }else{ 
    var lookup = { _id : String(req.body['_id']) }

    Booth.findOne( lookup , function(err, booth){
      if(err) res.status(409).send(_err.format(409, "Database Lookup Error.", {'err': err}));    
      else if(booth == null ) //Not found - 404
        res.status(404).send( _err.format(404, "No results found.", {'booth': booth}) );       
      else {
        console.log('booth', booth);
        for(key in req.body){
          if(key != "_id")
            booth[key]=req.body[key];  //add all the keys 
        }
        
        booth.save(function (err) {
          if (err)  res.status(409).send(_err.format(409, "Unable to update.", {'err': err}));    
          else      res.status(200).send( booth ); 
        });
      }
    });
  }
});



//Delete single booth
app.post('/booth/delete', function(req, res) { 

  if( !req.body['_id']  ){
    res.status(400).send(_err.format(400, "_id required.", {'req.body': req.body}));    

  }else{  // process and remove booth

    var lookup = { _id : String(req.body['_id']) }

    Booth.findOne(lookup, function(err, booth){
      if(err)  res.status(409).send(_err.format(409, "Database Lookup Error.", {'err': err}));    
      
      else if(booth == null ) //Not found - 404
        res.status(404).send( _err.format(404, "No results found.", {'booth' : booth}) );       
      
      else {
        booth.remove(function(err, data){         //delete the booth
          if(err) res.status(409).send(_err.format(409, "Database Lookup Error", {'err': err}));    
          else    res.status(200).send( JSON.stringify(lookup,null,4) + ' deleted.' ); 
        });
      }
    });
  }
});



//Return app
module.exports = app;



// TODO:

/*

    - Move functions out
    - Move models out

    all
      - comments
      - prevent injection
        // http://blog.websecurify.com/2014/08/hacking-nodejs-and-mongodb.html
      - error should not output password
      - bcrypt can slow down create, update, delete of user accounts. Restrict it's use to absolute minimum.

    /create
        //todo: check for valid email & password
        //todo: check if attendee already exists in db
        //todo: password hashing



*/



