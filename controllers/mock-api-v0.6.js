var express = require('express');
var app = express();

var root = "/mock-api/";
var version = "v0.6";


var api = root+version;

var mock = require('./data/mock-api-'+version+'.json');

/* GET home page. */
app.get(api, function(req, res, next) {
  res.render('mock-api-'+version+'.jade', { 
    title: 'Mock Api '+ version, 
    version: version,
    root: root,
    api: api
  });
});


/* GET booths listing. */
app.get(api+'/login', function(req, res, next) {
  res.send(mock.login);
});


/* 
  CRU for ATTENDEE, EVENT, BOOTH
*/

app.get(
  [ api+'/attendee/' , 
  api+'/attendee/create' ,
  api+'/attendee/update' ,  
  ] , function(req, res, next) {
  res.send(mock.attendee);
});

/* GET events listing. */
app.get(
  [api+'/event', 
  api+'/event/create',
  api+'/event/update'], 
  function(req, res, next) {
  res.send(mock.event);
});

/* GET events listing. */
app.get(
  [api+'/booth', 
  api+'/booth/create',
  api+'/booth/update'], 
  function(req, res, next) {
  res.send(mock.booths[0]);
});





/* /DELETE for ATTENDEE, EVENTS, BOOTHS */
app.get([
  api+'/event/delete',
  api+'/attendee/delete',
  api+'/booth/delete'  
  ],
 function(req, res, next) {
  res.send('{    "_id": "560a67eb3e3afd58f147f62d"} deleted.');
});



/*
  INDEXING - ATTENDEES, EVENTS, BOOTHS
*/

app.get(api+'/attendees', function(req, res, next) {  
  res.send(mock.attendees);
});


/* GET events listing. */
app.get(api+'/events', function(req, res, next) {
  res.send(mock.events);
});

/* GET booths listing. */
app.get(api+'/booths', function(req, res, next) {
  res.send(mock.booths);
});





/* GET events listing. */
app.get(api+'/event/interests', function(req, res, next) {
  res.send(mock.events);
});


/* GET events listing. */
app.get(api+'/event/register', function(req, res, next) {
  res.send(mock.register);
});







//Return app
module.exports = app;
