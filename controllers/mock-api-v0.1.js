var express = require('express');
var app = express();

var root = "/mock-api/";
var version = "v0.1";


var api = root+version;

var mock = require('./data/mock-api-'+version+'.json');

/* GET home page. */
app.get(api, function(req, res, next) {
  res.render('mock-api-'+version+'.jade', { 
    title: 'Mock Api '+ version, 
    version: version,
    root: root,
    api: api
  });
});


/* GET booths listing. */
app.get(api+'/login', function(req, res, next) {
  res.send(mock.login);
});


/* GET events listing. */
app.get(api+'/event', function(req, res, next) {
  res.send(mock.events);
});

/* GET booths listing. */
app.get(api+'/booth', function(req, res, next) {
  res.send(mock.booths);
});

app.get(api+'/attendee', function(req, res, next) {
  res.send(mock.attendee);
});

app.get(api+"/attendee/rec_booth", function(req, res, next) {
  res.send(mock.rec_booth);
});

app.get(api+"/attendee/interest", function(req, res, next) {
  res.send(mock.interest);
});

//Return app
module.exports = app;
