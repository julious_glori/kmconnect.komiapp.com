var express  = require('express');
var app      = express();
var _err     = require('../helpers/error.js');
var mongoose = require('mongoose');
var Event    = require('../models/event.js');


// route middleware - remove token
// so there's no need to keep dealing with it below.
app.use(function (req, res, next) {
  delete req.body.token;
  next();
});


function isArray(a) {
  return (!!a) && (a.constructor === Array);
};

function isObject(a) {
  return (!!a) && (a.constructor === Object)
}


function obj_walker(from, to){

  for(  var i in from){

    if(isObject(from[i])){
      to[i] = {}
      to[i] = obj_walker(from[i], to[i]);

    }else if (isArray(from[i])){
      to[i] = [];
      to[i] = obj_walker(from[i], to[i]);      

    }else{
      to[i] = from[i];
    }
  }

  return to;
}


//Create single event
app.post('/event/create', function(req, res) { 

  //new Event's data
  var newEvent = new Event();

  console.log('BEFORE: newEvent', newEvent);
  console.log('BEFORE: res.Body', req.body);
  obj_walker(req.body, newEvent);

//  newEvent = req.body;
  console.log('AFTER: newEvent', newEvent);
  
try {


  //save the event
  newEvent.save(function(err, data){
    if(err){
      console.error("err",err);
      res.status(409).send(_err.format(409, "Error while saving: ", {'err': err   }));
    }else{          
      res.status(200).send(data);          
    } 
  });


}catch(e){

  console.error('Saving error', e)
}


});



//Return event details
app.post('/event', function(req, res) {

  if( !req.body['_id']){ //error 400
    res.status(400).send(_err.format(400, "_id Missing.", {'req.body': req.body}));    

  }else{

    var lookup = { _id : String(req.body['_id']) }
    Event.findOne( lookup , function(err, event){
      
      if(err) //db Error - 400 
        res.status(409).send( _err.format(409, "Database Lookup Error.", {'err': err}) );      
      else if(event == null ) //Not found - 404
        res.status(404).send( _err.format(404, "No results found.", {'event': event}) ); 
      else
        res.status(200).send( event );         
    });
  }
});


//Update single event
app.post('/event/update', function(req, res) { 

  if( !req.body['_id'] ){
    res.status(400).send(_err.format(400, "_id missing.", {'req.body': req.body}));    

  }else{ 
    var lookup = { _id : String(req.body['_id']) }

    Event.findOne( lookup , function(err, event){
      if(err) res.status(409).send(_err.format(409, "Database Lookup Error.", {'err': err}));    
      else if(event == null ) //Not found - 404
        res.status(404).send( _err.format(404, "No results found.", {'event': event}) );       
      else {
        console.log('event', event);
        for(key in req.body){
          if(key != "_id")
            event[key]=req.body[key];  //add all the keys 
        }
        
        event.save(function (err) {
          if (err)  res.status(409).send(_err.format(409, "Unable to update.", {'err': err}));    
          else      res.status(200).send( event ); 
        });
      }
    });
  }
});



//Delete single event
app.post('/event/delete', function(req, res) { 

  if( !req.body['_id']  ){
    res.status(400).send(_err.format(400, "_id required.", {'req.body': req.body}));    

  }else{  // process and remove event

    var lookup = { _id : String(req.body['_id']) }

    Event.findOne(lookup, function(err, event){
      if(err)  res.status(409).send(_err.format(409, "Database Lookup Error.", {'err': err}));    
      
      else if(event == null ) //Not found - 404
        res.status(404).send( _err.format(404, "No results found.", {'event' : event}) );       
      
      else {
        event.remove(function(err, data){         //delete the event
          if(err) res.status(409).send(_err.format(409, "Database Lookup Error", {'err': err}));    
          else    res.status(200).send( JSON.stringify(lookup, null , 4) + ' deleted.' ); 
        });
      }
    });
  }
});



//Return app
module.exports = app;



// TODO:

/*

    - Move functions out
    - Move models out

    all
      - comments
      - prevent injection
        // http://blog.websecurify.com/2014/08/hacking-nodejs-and-mongodb.html
      - error should not output password
      - bcrypt can slow down create, update, delete of user accounts. Restrict it's use to absolute minimum.

    /create
        //todo: check for valid email & password
        //todo: check if attendee already exists in db
        //todo: password hashing



*/



