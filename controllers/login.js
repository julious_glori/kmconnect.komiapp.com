var express   = require('express');
var app       = express();
var config    = require("../config.js");
var _err      = require('../helpers/error.js');  

var mongoose  = require('mongoose');
var Attendee  = require('../models/user.js');
var Audit     = require('../models/audit.js');

var jwt       = require("jsonwebtoken");

//Login
app.post('/login', function(req, res) { 

  // console.log('req.body', req.body);

  if( !req.body.email || !req.body.password){
    res.status(400).send(_err.format(400, "Missing required fields.", {'req.body': req.body}));    
  }else{

    //lookup the attendee
    Attendee.findOne({email: String(req.body.email)}, function(err,user) {

      if (err ) res.status(409).send(_err.format(400, "Lookup Error. ", { error: err}));
      else if (!user) {
        // can create attendee if provided an adminToken
        if (req.body.kmconnectToken && (req.body.kmconnectToken === config.admin.token)) {
          /** user doesnt exists, creating user here */
          var newAttendee = new Attendee();

          newAttendee['username'] = String(req.body['email']);
          newAttendee['password'] = String(req.body['password']);
          newAttendee['email'] = String(req.body['email']);

          //save the attendee
          newAttendee.save(function(err, user){
            if (err) {
              /*console.error("err",err);*/
              res.status(409).send(_err.format(409, "Error while saving: ", { 'err': err, err_data: user, req_data: req.body   }));
            } else {

              var token = jwt.sign(user, config.secret.token, { expiresInMinutes: config.session.duration });

              ////  Lup Yuen 11 Nov 2015: Added to fix iOS deviceToken not saved when sending ContentType=application/json
              //create new entry in login-audit
              var newAudit = new Audit();

              newAudit['username']    = String(req.body['email']);
              newAudit['deviceToken'] = req.body['deviceToken'] ? String(req.body['deviceToken']) : '';

              newAudit.save();

              ////  Lup Yuen 11 Nov 2015: End of fix

              user = user.toObject();
              delete user.password;
              user.sessionid = token;

              res.status(200).send(user);
            }
          });

        }
        else {
          res.status(404).send(_err.format(404, "User Not Found.", { data: user}));     
        }

      }
      else {
        //does password match?
        user.comparePassword(String(req.body.password), function(err,isMatch) {
          if (!isMatch) res.status(401).send(_err.format(401, "Attempt failed to login with " + user.username));
          else {
            console.log('\ncomparePassword isMatch: '+ isMatch);

            var token = jwt.sign(user, config.secret.token, { expiresInMinutes: config.session.duration });

            //create new entry in login-audit
            var newAudit = new Audit();

            newAudit['username']    = String(req.body['email']);
            newAudit['deviceToken'] = req.body['deviceToken'] ? String(req.body['deviceToken']) : '';

            newAudit.save();

            user = user.toObject();
            delete user.password;
            user.sessionid = token;
            res.status(200).json(user);              
          }
        });

      }
    });
  }
});


//Return app
module.exports = app;

