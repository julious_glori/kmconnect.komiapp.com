var express = require('express');
var app = express();

var root = "/mock-api/";
var version = "v0.4";


var api = root+version;

var mock = require('./data/mock-api-'+version+'.json');

/* GET home page. */
app.get(api, function(req, res, next) {
  res.render('mock-api-'+version+'.jade', { 
    title: 'Mock Api '+ version, 
    version: version,
    root: root,
    api: api
  });
});


/* GET booths listing. */
app.get(api+'/login', function(req, res, next) {
  res.send(mock.login);
});

/* GET events listing. */
app.get(
  [api+'/event', 
  api+'/event/create',
  api+'/event/update'], 
  function(req, res, next) {
  res.send(mock.event);
});



/* GET events listing. */
app.get(api+'/event/delete', function(req, res, next) {
  res.send('{    "_id": "560a67eb3e3afd58f147f62d"} deleted.');
});



/* GET events listing. */
app.get(api+'/events', function(req, res, next) {
  res.send(mock.events);
});

/* GET events listing. */
app.get(api+'/event/interests', function(req, res, next) {
  res.send(mock.events);
});

/* GET booths listing. */
app.get(api+'/booths', function(req, res, next) {
  res.send(mock.booths);
});

app.get(api+'/attendees', function(req, res, next) {
  res.send(mock.attendee);
});

app.get(api+"/attendee/recommended_booths", function(req, res, next) {
  res.send(mock.rec_booth);
});

app.get(api+"/attendee/interests", function(req, res, next) {
  res.send(mock.interest);
});


//Return app
module.exports = app;
