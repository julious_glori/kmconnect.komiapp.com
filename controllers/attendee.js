var express   = require('express');
var app       = express();

var _err       = require('../helpers/error.js');  

var mongoose  = require('mongoose');
var Attendee  = require('../models/user.js');





//Return attendee details
app.post('/attendees', function(req, res) {
  Attendee.find({}, function(err, data){

    if(err)                   res.status(409).send( _err.format(409, "Database Lookup Error.", {'err': err}) );      
    else if(data == null)     res.status(404).send( _err.format(404, "No results found.", {data: data}) )
    
    else {
      for (var u in data) {
        var user = data[u];
        user = user.toObject();       
        delete user['password'];
        console.log('user aft pass del: ', user);
        data[u] = user;
      }
      res.status(200).send( data );                   
    } 

  });

});




//Return attendee details
app.post('/attendee', function(req, res) {

  if( !req.body['_id'] && !req.body['username']){ //error 400
    res.status(400).send(_err.format(400, "ID or Username required.", {'req.body': req.body}));    

  }else{

    var key  = req.body['_id'] ? '_id' : 'username';
    var val  = req.body['_id'] ? req.body['_id'] : req.body['username'];
    
    var lookup = {};
    lookup[key]  = val;

    Attendee.findOne(lookup, function(err, user){
      
      if(err) //db Error - 400 
        res.status(409).send( _err.format(409, "Database Lookup Error.", {'err': err}) );      
      else if(user == null ) //Not found - 404
        res.status(404).send( _err.format(404, "No results found.", {'user': user}) ); 

      else{
        //success - 200
        user = user.toObject();
        delete user.password;
        res.status(200).send( user );         
      } 

    });
  }
});


//Update single attendee
app.post('/attendee/update', function(req, res) { 
  

  if( !req.body['_id'] && !req.body['username']){
    res.status(400).send(_err.format(400, "ID or Username required.", {'req.body': req.body}));    

  }else{ 

    var key  = req.body['_id'] ? '_id': 'username';
    var val  = req.body['_id'] ? req.body['_id'] : req.body['username'];
    
    var lookup = {};
    lookup[key]  = val;

    Attendee.findOne(lookup, function(err, user){
      if(err) res.status(409).send(_err.format(409, "Database Lookup Error.", {'err': err}));    
      else if(user == null ) //Not found - 404
        res.status(404).send( _err.format(404, "No results found.", {'user': user}) );       
      else {

        console.log('user', user);
        for(key in req.body){
          if(key != "_id")
            user[key]=req.body[key];  //add all the keys 
        }
        
        user.save(function (err) {
          if (err)  res.status(409).send(_err.format(409, "Unable to update.", {'err': err}));    
          else{
                //success - 200
            user = user.toObject();
            delete user.password;
            res.status(200).send( user ); 
          }      
        });

      }
    });
  }
});



//Delete single attendee
app.post('/attendee/delete', function(req, res) { 


  if( !req.body['_id'] &&  !req.body['username'] ){
    res.status(400).send(_err.format(400, "ID or Username required.", {'req.body': req.body}));    

  }else{  // process and create attendee

    var key  = req.body['_id'] ? '_id': 'username';
    var val  = req.body['_id'] ? req.body['_id'] : req.body['username'];

    var lookup = {};
    lookup[key]  = val;

    Attendee.findOne(lookup, function(err, user){
      if(err) res.status(409).send(_err.format(409, "Database Lookup Error.", {'err': err}));    
      else if(user == null ) //Not found - 404
        res.status(404).send( _err.format(404, "No results found.", {'user': user}) );       
      else {

        //delete the attendee
        user.remove(function(err, data){
          if(err) res.status(409).send(_err.format(409, "Database Lookup Error", {'err': err}));    
          else{
            //success - 200
            user = user.toObject();
            delete user.password;
            res.status(200).send( user ); 
          } 
        });

      }
    });


  }
});



//Return app
module.exports = app;



// TODO:

/*

    - Move functions out
    - Move models out

    all
      - comments
      - prevent injection
        // http://blog.websecurify.com/2014/08/hacking-nodejs-and-mongodb.html
      - error should not output password
      - bcrypt can slow down create, update, delete of user accounts. Restrict it's use to absolute minimum.

    /create
        //todo: check for valid email & password
        //todo: check if attendee already exists in db
        //todo: password hashing



*/



