var express  = require('express');
var app      = express();
var _err     = require('../helpers/error.js');
var mongoose = require('mongoose');
var Event    = require('../models/event.js');





//Return all events
app.post('/events', function(req, res) {
  Event.find({}, function(err, events){    
    if(err) res.status(409).send( _err.format(409, "Database Lookup Error.", {'err': err}) );      
    else    res.status(200).send( events );         
  });
});


//Return app
module.exports = app;


