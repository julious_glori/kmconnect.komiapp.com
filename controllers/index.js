var express = require('express');
var app = express();

/* GET home page. */
app.get('/', function(req, res, next) {

console.log(app.stack);

  res.render('index', 
  { 
    title: 'SEED2 CMS', 
    api: app._router.stack
  });
});


module.exports = app;
