var express       = require('express');
var path          = require('path');
var favicon       = require('serve-favicon');
var logger        = require('morgan');
var cookieParser  = require('cookie-parser');
var bodyParser    = require('body-parser');
var mongoose      = require('mongoose');
var jwt           = require('jsonwebtoken');

require('pretty-error').start(); // 

var config    = require('./config.js');


var app = express();

mongoose.connect(config.db.DEVELOPMENT, function(err){
  if(err) throw err;
});

var _err       = require('./helpers/error.js');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.text()); 
app.use(bodyParser.json());
app.use(cookieParser());

//Use /public as DMZ
app.use(express.static(path.join(__dirname, 'public')));



//Instead of using routes, 
//we'll use controllers which will determine routes
//App.js code should be glue code only
app.use(require('./controllers'));
app.use(require('./controllers/mock-api-v0.1.js'));
app.use(require('./controllers/mock-api-v0.2.js'));
app.use(require('./controllers/mock-api-v0.3.js'));
app.use(require('./controllers/mock-api-v0.4.js'));
app.use(require('./controllers/mock-api-v0.5.js'));
app.use(require('./controllers/mock-api-v0.6.js'));

//Routes that don't need authentication
app.use(require('./controllers/login.js'));
app.use(require('./controllers/register.js'));
app.use(require('./controllers/events.js'));

//Authentication 
app.post('*', function(req, res, next) {

  //allow deletion for tests without security check ---- bad I think
  //better to take out these kind of hacks and keep main code separate from tests
  if(( req.body.username == 'MOCHA_TEST_USER' || req.body.username == 'MOCHA_TEST_BOOTH_USER' ) && req.path == '/attendee/delete'){
    next();
    return;
  } 
  
  var token = req.body.token;
  jwt.verify(token, config.secret.token , function(err, decoded) {

    var debug = { err: err, decoded: decoded, req_body: req.body }  

    if(err) res.status(401).send(_err.format(401, "Invalid credentials. " , debug));
    else next();
  });    

});

//Authenticated Routes
app.use(require('./controllers/attendee.js'));
app.use(require('./controllers/event.js'));
app.use(require('./controllers/booth.js'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});


// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


app.listen(9001);

module.exports = app;
