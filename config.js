module.exports = {
  db:{
    TEST: 'mongodb://localhost/test',
    DEVELOPMENT:  'mongodb://localhost/seed2',
    PRODUCTION: 'mongodb://localhost/seed2-production'  
  },
  secret:{
    token: "sqqZ1[5v+;QJRqV5n!ULm*&ymz3@OOp645QQWF\"0pM^6=%W~IaK+eJDq8[FG4U|tk$9[C4QRA9HmHnl~o,*sog8-w}n<SKE=Hxc9<)KmLby)p!bD&f,?lN_8\"HTObhA8",
    test_delete_token: "PbuyXaDA*`D}(wp*{L]zh<'<q`S.B%b>y7FzxK%~Dcd"
  },
  session:{
    duration: 60  //in Mins
  },
  admin: {
    token: 'admin-kmconnect123'
  }
}