var should      = require("chai").should();
var request     = require('request');
var bodyParser  = require('body-parser');


//jshint

var svr         = "http://localhost:9001";  //  Local server
var testUser    = { 

    username: "MOCHA_TEST_USER", 
    email: "mocha-test@mocha-test.com", 
    password: "123PASSWORD123",
    first_name: "foo_first_name",
    last_name: "foo_last_name",
    job_title: "Business Systems Director",
    company: "ACME Holdings Ltd",
    country: "Indonesia",    
    phone: "+863452344513",
    avatar: "http://cards.komiapp.com/konica/assets/people.png",
    event_registration_data: { 'haha': 'hehe' }

  };
var testUserId;

describe("Creating new User Accounts: ", function(){

  before("delete test user if it somehow still exists", function(done){    
    //delete first test user
    request.post(svr + '/attendee/delete', { form: {username: 'MOCHA_TEST_USER' } } , function (error, res, body) {
      //delete second test user
      request.post(svr + '/attendee/delete', { form: {username: 'MOCHA_TEST_USER2' } } , function (error, res, body) {
        done();
      });      
    });       
  });

  //TODO: Password should not return in errors
  describe("/attendee/create call", function(){

    it('should throw 400[Bad Request] on empty query', function (done) {
      request.post(svr + '/attendee/create', function (error, res, body) {
        
        res.statusCode.should.equal(400);
        done();
      })
    });

    //minimum required parameters - username, password
    it('should throw 400[Bad Request] if username OR password not present', function (done) {
      var data = { form: {username:'', password:''}};
      request.post(svr + '/attendee/create', data , function (error, res, body) {
        res.statusCode.should.equal(400);  //Error Status
        done();
      })
    });    

    //create user once - should pass
    it('should return 200[OK] and new user details on sending { username }, { password}  and { other details }', function (done) {
      var data = { form: testUser};
      request.post(svr + '/attendee/create' , data , function (error, res, body) {



        if(error) throw error;

        should.exist(res);
        console.log('statusCode:', res.statusCode);
        res.statusCode.should.equal(200);

        res.body.should.contain('_id');

        res.body.should.contain('username');
        res.body.should.contain(testUser.username); 

        res.body.should.not.contain('password');     
        res.body.should.not.contain(testUser.password);    

        // Store the ID for futher testing below
        // and sessionid
        if(res.statusCode == 200) {
          testUserId = JSON.parse(body)["_id"];

          request.post(svr + '/login' , data , function (error, res, body) {
            testUser['token'] = String( JSON.parse(body)['sessionid']  );
            done();      
          });

        }   


      });
    });    


    //create same user again - should fail
    it('should return 409[Conflict Occurred] if user already exists', function (done) {
      var data = { form: testUser};
      request.post(svr + '/attendee/create' , data , function (error, res, body) {
        should.exist(res);
        res.statusCode.should.equal(409);
        done();      
      });
    });    


  });
});

describe("Reading attendee data", function(){
  describe("/attendee call", function(){
    it('should return 400[Bad Request] if { _id } or { username } not sent', function (done) {
      request.post(svr + '/attendee', { form: { token : testUser['token'] } } , function (error, res, body) {
        res.statusCode.should.equal(400);
        done();
      })
    });
    it('should return 200[OK] and attendee data for { _id }', function (done) {
      var data = { form: {_id: testUserId , token : testUser['token']} };      
      request.post(svr + '/attendee', data , function (error, res, body) {

        var pBody = JSON.parse(body);

        should.not.exist(pBody['password']);

        pBody['username'].should.equal(testUser.username);
        pBody['first_name'].should.equal(testUser.first_name);
        pBody['last_name'].should.equal(testUser.last_name);
        pBody['job_title'].should.equal(testUser.job_title);
        pBody['company'].should.equal(testUser.company);
        pBody['country'].should.equal(testUser.country);
        pBody['phone'].should.equal(testUser.phone);
        pBody['avatar'].should.equal(testUser.avatar);

        // should.exist(pBody['event_registration_data']);


        res.statusCode.should.equal(200);
        done();
      })
    });   
    it('should return 200[OK] and attendee data for { username }', function (done) {
      var data = { form: {username: testUser.username , token : testUser['token']  } };      
      request.post(svr + '/attendee', data , function (error, res, body) {
        res.body.should.contain('_id');
        res.statusCode.should.equal(200);
        done();
      })
    });  
    it('should never return the user\'s password', function(done){
      var data = { form: {username: testUser.username, token : testUser['token'] } };      
      request.post(svr + '/attendee', data , function (error, res, body) {
        res.body.should.not.contain('password');
        done();
      })      
    }); 

  });
});

describe("Updating attendee data", function(){
  describe("/attendee/update call", function(){

    it('should return 400[Bad Request] on empty request', function (done) {
      request.post(svr + '/attendee/update', { form :  { token : testUser['token'] }   } , function (error, res, body) {
        res.statusCode.should.equal(400);
        done();      
      });
    });



    it('should return 200[OK] & data, on updating via { _id }', function (done) {

      request.post(svr + '/attendee/update', {form : { _id: testUserId, email: 'test1', token : testUser['token'] } } , function (error, res, body) {
        res.statusCode.should.equal(200);
        JSON.parse(body)["email"].should.equal('test1');
        done();      
      });

    });    

    it('should return 200[OK] & data, on updating via { username }', function (done) {

      request.post(svr + '/attendee/update', {form : { username: testUser.username, email: 'test2', token : testUser['token'] } } , function (error, res, body) {
        res.statusCode.should.equal(200);
        JSON.parse(body)["email"].should.equal('test2');
        done();      
      });

    });    

    it('should return 409[Conflict Occurred] on invalid { _id }', function (done) {

      request.post(svr + '/attendee/update', {form : { _id: '00000000000', token : testUser['token']} } , function (error, res, body) {
        res.statusCode.should.equal(409);
        done();      
      });

    });

    it('should return 404[Not Found] on invalid { username }', function (done) {
      request.post(svr + '/attendee/update', {form : { username: 9879, token : testUser['token']} } , function (error, res, body) {
        res.statusCode.should.equal(404);
        done();      
      });
    });
    it('should never return the user\'s password', function(done){
      var data = { form: {username: testUser.username, token : testUser['token'] } };      
      request.post(svr + '/attendee/update', data , function (error, res, body) {
        res.body.should.not.contain('password');
        done();
      })      
    }); 



  });
});


describe("Deleting User Accounts: ", function(){

  describe("/attendee/delete call", function(){

    it('should return 400[Bad Request] if { _id } or { username } missing', function (done) {
      request.post(svr + '/attendee/delete', {form: {  token : testUser['token'] } } , function (error, res, body) {
        res.statusCode.should.equal(400);
        done();
      })
    });

    //delete user accounts
    it('should return 200[OK] when { _id } is sent', function (done) {

      if(testUserId){
        request.post(svr + '/attendee/delete', {form: {_id: testUserId , token : testUser['token']} } , function (error, res, body) {
          res.statusCode.should.equal(200);
          done();
        });        
      }else{
        done( new Error('Test Attendee ID[_id] is missing. Unable to test /attendee/delete api call') );  
      }
    });

    //delete user accounts
    it('should return 200[OK] when { username } is sent', function (done) {

      //create user
      request.post(svr + '/attendee/create', { form: { username: 'MOCHA_TEST_USER2', email: 'adsf@adsf.com', password: 'asd' }} , function (error, res, body) {
        //res.statusCode.should.equal(200);

        //delete user by username
        request.post(svr + '/attendee/delete', { form: { username: 'MOCHA_TEST_USER2' , token : testUser['token']}} , function (error, res, body) {
          res.body.should.not.contain('password');
          res.statusCode.should.equal(200);
          done();
        });    
      });  
    });
  });
});



describe("Reading Attendee Data: ", function(){
  describe("/attendee call", function(){
    it('should return 404[Not Found] after attendee is deleted', function (done) {

      var data = { form: {_id: testUserId, token : testUser['token']} };      
      request.post(svr + '/attendee', data , function (error, res, body) {
        res.statusCode.should.equal(404);
        done();
      });
    });
  });
});

/*
describe("Reading attendees data", function(){
  describe("/attendees call", function(){


    it('should return 200[OK] and attendees data', function (done) {
      var data = { form: {token : testUser['token']} };      
      request.post(svr + '/attendees', data , function (error, res, body) {

        res.body.should.contain('_id');

        res.body.should.contain('username');
       // res.body.should.contain(testUser.username); 






        res.body.should.not.contain('password');     
        res.body.should.not.contain(testUser.password);    

        JSON.parse(res.body).length.should.be.above(1);
      
        //console.log(JSON.parse(res.body));

        res.statusCode.should.equal(200);
        done();
      })
    });   
  });
});
*/



/*
describe("Becrypt - Password Encryption Function", function(){

  it('/update - should not run bcrypt if password is not updated', function (done) {

    done(new Error(' ..... EXPECTDE FAIL .....'));      

  });

  it('/update : should run bcrypt if password is updated', function (done) {

    done(new Error(' ..... EXPECTDE FAIL .....'));      

  });


  it('/delete : should not run bcrypt', function (done) {

    done(new Error(' ..... EXPECTDE FAIL .....'));      

  });

});

*/