var should      = require("chai").should();
var request     = require('request');
var bodyParser  = require('body-parser');


//jshint

var svr         = "http://localhost:9001";  //  Local server
var testUser    = { username: "MOCHA_TEST_LOGIN_USER", email: "mocha-test@mocha-test.com", password: "123PASSWORD123"};
var testUserId;


// Does POST req to urlArr urls
// stores status code in resArr 
var reqFunc = function(urlArr, resArr, cb){

  var url = urlArr[0];

  request.post(svr + url , function (error, res, body) {

    resArr[url]=res.statusCode;
    urlArr.shift();

    if(urlArr.length > 0) reqFunc(urlArr, resArr, cb);
    else cb();
    
  });       
}

/*
  Authenticated Routes. 
  Exceptions: /attendee/create, /login
*/

var authRoutes =  
  ['/attendee', '/attendee/update', '/attendee/delete',
  '/event', '/event/create', '/event/update', '/event/delete',  
  '/booth', '/booth/create', '/booth/update', '/booth/delete', 
  '/booths'];

var authCodes = {}



describe('Testing authenticated routes',function(){
  it('', function(done){
    reqFunc(authRoutes, authCodes, function(){
      for(i in authCodes){
        console.log('         Route %s, status: %s', i, authCodes[i]);
        authCodes[i].should.equal(401);            
      }
      done();
    });
  });
})

var openRoutes = 
  ['/attendee/create', 
  '/login', 
  '/events'];

var openRouteCodes = {}

describe('Testing open routes',function(){
  it('', function(done){
    reqFunc(openRoutes, openRouteCodes, function(){
      for(i in openRouteCodes){
        console.log('         Route %s, status: %s', i, openRouteCodes[i]);
        openRouteCodes[i].should.not.equal(401);            
      }
      done();
    });
  });
})



  

















