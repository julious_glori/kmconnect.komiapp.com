var should      = require("chai").should();
var request     = require('request');
var bodyParser  = require('body-parser');
var config      = require('../config.js');

//jshint

var svr         = "http://localhost:9001";  //  Local server
var testEvent    = { name: "MOCHA_TEST_EVENT", description: "mocha-test event", thumbnail: "http://test.com/test.png"};
var testEventId;
var testEventUser = { username: "MOCHA_TEST_EVENT_USER", password: "mocha-test event"};
var delToken = config.secret.test_delete_token
var token;

before("Delete, create & login test user. Return Test User sessionid token", function(done){    

  this.timeout(5000);    //Creating user can take a little longer due to bcrypt hashing

  //delete old test user
  request.post(svr + '/attendee/delete', {form: testEventUser} , function (error, res, body) {

    //create new test user
    request.post(svr + '/attendee/create', { form: testEventUser}, function (error, res, body) {

      //login & return the token    
      request.post(svr + '/login', { form: testEventUser}, function (error, res, body) {
        token = JSON.parse(body).sessionid;
        testEvent['token'] = token;

        var data = { form: testEvent};
        //create 2 events
        request.post(svr + '/event/create' , data , function (error, res, body) {
          request.post(svr + '/event/create' , data , function (error, res, body) {
            done();
          });
        });
          
      });
    });
  });
});

describe("Reading events data", function(){
  describe("/events call", function(){


    it('should return 200[OK] and events data', function (done) {
      var data = { form:  { _id : testEventId, token: token } };      
      request.post(svr + '/events', data , function (error, res, body) {

        res.body.should.contain('_id');

        res.body.should.contain('name');
        res.body.should.contain(testEvent.name); 

        res.body.should.contain('description');
        res.body.should.contain(testEvent.description); 

        res.body.should.contain('thumbnail');
        res.body.should.contain(testEvent.thumbnail); 


        res.statusCode.should.equal(200);
        done();
      })
    });   
  });
});
