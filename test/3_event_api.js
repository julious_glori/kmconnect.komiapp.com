var should      = require("chai").should();
var request     = require('request');
var bodyParser  = require('body-parser');
var config      = require('../config.js');

//jshint

var svr         = "http://localhost:9001";  //  Local server
var testEvent    = { 
  name: "MOCHA_TEST_EVENT", 
  description: "mocha-test event", 
  thumbnail: "http://test.com/test.png",
  floorplan: [
    { _id : 1, 
      label: "test-event image 1",
      image: 'http://test-event-image/img.png'
     }
  ],
  tags: {
    
    show: [ { _id: 10, label: "test-show-1" }, { _id: 20, label: "test-show-2" }]

  } 

};
var testEventId;
var testEventUser = { username: "MOCHA_TEST_EVENT_USER", password: "mocha-test event", email: 'mocha'};
var delToken = config.secret.test_delete_token
var token;

before("Delete, create & login test user. Return Test User sessionid token", function(done){    

  this.timeout(5000);    //Creating user can take a little longer due to bcrypt hashing

  //delete old test user
  request.post(svr + '/attendee/delete', {form: testEventUser} , function (error, res, body) {

    //create new test user
    request.post(svr + '/attendee/create', { form: testEventUser}, function (error, res, body) {

      //login & return the token    
      request.post(svr + '/login', { form: testEventUser}, function (error, res, body) {
        token = JSON.parse(body).sessionid;
        testEvent['token'] = token;

        //remove the test Event
        request.post(svr + '/event/delete', { form: testEvent}, function (error, res, body) {
          //remove the test Event

          done();

        });
      });
    });
  });
});


describe("Creating new Event: ", function(){

  describe("/event/create call", function(){

    it('should throw 401[Unauthorized] on empty query', function (done) {
      request.post(svr + '/event/create', function (error, res, body) {
        
        res.statusCode.should.equal(401);
        done();
      })
    });

    it('should throw 401[Unauthorized] if name or desc not present', function (done) {
      var data = { form: {name:'', description:''}};
      request.post(svr + '/event/create', data , function (error, res, body) {
        res.statusCode.should.equal(401);  //Error Status
        done();
      })
    });    

    //create user once - should pass
    it('should return 200[OK] and new event details on sending { name }, { description }  and { other details }', function (done) {
      var data = { form: testEvent};
      request.post(svr + '/event/create' , data , function (error, res, body) {

        if(error) throw error;

        should.exist(res);
        
        res.statusCode.should.equal(200);

        var pBody = JSON.parse(res.body);

        should.exist(pBody['_id']);

        pBody['name'].should.equal(testEvent.name);
        pBody['description'].should.equal(testEvent.description); 
        pBody['thumbnail'].should.equal(testEvent.thumbnail); 
        // should.exist(pBody['floorplan'][0]['_id']);
        // should.exist(pBody['tags']['show'][0]);



/*
        pBody['tags']['show'][0]['_id'].should.equal( testEvent['tags']['show'][0]['_id'] ); 
        pBody['tags']['show'][0]['label'].should.equal( testEvent['tags']['show'][0]['label'] ); 
        pBody['tags']['show'][1]['_id'].should.equal( testEvent['tags']['show'][1]['_id'] ); 
        pBody['tags']['show'][1]['label'].should.equal( testEvent['tags']['show'][1]['label'] ); 


        pBody['tags']['interest'][0]['_id'].should.equal( testEvent['tags']['interest'][0]['_id'] ); 
        pBody['tags']['interest'][0]['label'].should.equal( testEvent['tags']['interest'][0]['label'] ); 
        pBody['tags']['interest'][1]['_id'].should.equal( testEvent['tags']['interest'][1]['_id'] ); 
        pBody['tags']['interest'][1]['label'].should.equal( testEvent['tags']['interest'][1]['label'] ); 


        pBody['tags']['trend'][0]['_id'].should.equal( testEvent['tags']['trend'][0]['_id'] ); 
        pBody['tags']['trend'][0]['label'].should.equal( testEvent['tags']['trend'][0]['label'] ); 
        pBody['tags']['trend'][1]['_id'].should.equal( testEvent['tags']['trend'][1]['_id'] ); 
        pBody['tags']['trend'][1]['label'].should.equal( testEvent['tags']['trend'][1]['label'] ); 

*/

        if(res.statusCode == 200)   //Store the ID for futher testing below
          testEventId = pBody["_id"];

        done();      
      });
    });    
  });
});

describe("Reading event data", function(){
  describe("/event call", function(){
    it('should return 400[Bad Request] if { _id } not sent', function (done) {
      request.post(svr + '/event', {form: { token: token} }, function (error, res, body) {
        res.statusCode.should.equal(400);
        done();
      })
    });

    it('should return 200[OK] and event data for { _id }', function (done) {
      var data = { form:  { _id : testEventId, token: token } };      
      request.post(svr + '/event', data , function (error, res, body) {

        var pBody = JSON.parse(res.body);

        should.exist(pBody['_id']);
        pBody['name'].should.contain(testEvent.name);
        pBody['description'].should.contain(testEvent.description); 
        pBody['thumbnail'].should.contain(testEvent.thumbnail); 


        res.statusCode.should.equal(200);
        done();
      })
    });   
  });
});

describe("Updating event data", function(){
  describe("/event/update call", function(){

    it('should return 400[Bad Request] on empty request', function (done) {
      request.post(svr + '/event/update',  { form : {  token : token } } , function (error, res, body) {
        res.statusCode.should.equal(400);
        done();      
      });
    });



    it('should return 200[OK] & data, on updating via { _id }', function (done) {

      request.post(svr + '/event/update', {form : { _id: testEventId, thumbnail: "changed_thumbnail" , token: token } } , function (error, res, body) {
        res.statusCode.should.equal(200);
        JSON.parse(body)["thumbnail"].should.equal('changed_thumbnail');
        done();      
      });

    });    


    it('should return 409[Conflict Occurred] on invalid { _id }', function (done) {

      request.post(svr + '/event/update', {form : { _id: '00000000000' , token: token } } , function (error, res, body) {
        res.statusCode.should.equal(409);
        done();      
      });

    });

  });
});


describe("Deleting Events: ", function(){

  describe("/event/delete call", function(){

    it('should return 400[Bad Request] if { _id }  missing', function (done) {
      request.post(svr + '/event/delete', { form: { token : token} } , function (error, res, body) {
        res.statusCode.should.equal(400);
        done();
      })
    });

    //delete events
    it('should return 200[OK] when { _id } is sent', function (done) {

      if(testEventId){
        request.post(svr + '/event/delete', { form: { _id: testEventId, token: token } } , function (error, res, body) {
          res.statusCode.should.equal(200);
          done();
        });        
      }else{
        done( new Error('Test Event ID[_id] is missing. Unable to test /event/delete api call') );  
      }
    });
  });
});



describe("Reading Event Data: ", function(){
  describe("/event call", function(){
    it('should return 404[Not Found] after event is deleted', function (done) {

      var data = { form: {_id: testEventId, token: token} };      
      request.post(svr + '/event', data , function (error, res, body) {
        res.statusCode.should.equal(404);
        done();
      });
    });
  });
});
