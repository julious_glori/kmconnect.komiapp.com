var should      = require("chai").should();
var request     = require('request');
var bodyParser  = require('body-parser');


//jshint

var svr         = "http://localhost:9001";  //  Local server
var testUser    = { username: "MOCHA_TEST_LOGIN_USER", email: "mocha-test@mocha-test.com", password: "123PASSWORD123"};
var testUserId;

describe("Login Api",function(){
  describe("Logging in",function(){
    describe("/login call",function(){
      
      //test setup, create user.
      before("delete & create test user", function(done){  
        //delete test user
        request.post(svr + '/attendee/delete', { form: testUser } , function (error, res, body) {
          // console.log('\n\nDeletion results: '+ body);
        //create test user
          request.post(svr + '/attendee/create', { form: testUser } , function (error, res, body) {
            // console.log('\n\nCreated USER details: '+ body);
            done();
          }); 
        });          
      });

      it("should throw 400 on empty request",function(done){
        request.post(svr + '/login' , function (error, res, body) {
          res.statusCode.should.equal(400);
          done();
        });    
      });

      it("requires both username and password", function(done){
        request.post(svr + '/login', {form: {username: testUser.username, password: ""}} , function (error, res, body) {
          res.statusCode.should.equal(400);
          request.post(svr + '/login', {form: {username: "", password: testUser.password}} , function (error, res, body) {
            res.statusCode.should.equal(400);
            done();
          });          
        });
      });

      it("should login on valid credentials", function(done){
        request.post(svr + '/login', {form: {email: testUser.email, password: testUser.password}} , function (error, res, body) {
          res.statusCode.should.equal(200);
          done();
        });
      });
              
      it("should fail login on invalid credentials", function(done){
        request.post(svr + '/login', {form: {email: testUser.email , password: testUser.password + 'asd'}} , function (error, res, body) {
          res.statusCode.should.equal(401);
          done();
        });
      });
      it("should return id, sessionid on successful login", function(done){
        request.post(svr + '/login', {form: {email: testUser.email, password: testUser.password}} , function (error, res, body) {
          res.body.should.contain('_id');          
          res.body.should.contain('sessionid');
          done();
        });
      });
      it('should never return the user\'s password', function(done){
        var data = { form: {username: testUser.username } };      
        request.post(svr + '/login', {form: {email: testUser.email, password: testUser.password}} , function (error, res, body) {
          res.body.should.not.contain('password');
          done();
        })      
      });       
    });
  });
  describe("Logging out",function(){
  });
});