var should      = require("chai").should();
var request     = require('request');
var bodyParser  = require('body-parser');
var config      = require('../config.js');

//jshint

var svr         = "http://localhost:9001";  //  Local server
var testBooth    = { 
    name: "MOCHA_TEST_BOOTH",
    title: "Booth Title", 
    description: "mocha-test booth", 
    thumbnail: "http://test.com/test.png",
    image: 'http://booth.com/booth.png',
    address: '40 Raffles Boulevard, Singapore',
    email: 'mocha-booth@booth.com',
    phone: '+65 0937459345',
    url: 'http://url.url.com',

    coordinates: { 
      floorplan_id: '123123',
      x: 100,
      y: 50
    }
};

var testBoothId;
var testBoothUser = { username: "MOCHA_TEST_BOOTH_USER", password: "mocha-test booth", email: "mocha-email"};
var delToken = config.secret.test_delete_token
var token;
var testEvent;

before("Delete, create & login test user. Return Test User sessionid token", function(done){    

  this.timeout(5000);    //Creating user can take a little longer due to bcrypt hashing

  //delete old test user
  request.post(svr + '/attendee/delete', {form: testBoothUser} , function (error, res, body) {

    //create new test user
    request.post(svr + '/attendee/create', { form: testBoothUser}, function (error, res, body) {

      //login & return the token    
      request.post(svr + '/login', { form: testBoothUser}, function (error, res, body) {
        token = JSON.parse(body).sessionid;
        testBooth['token'] = token;


        //remove the test Booth
        request.post(svr + '/booth/delete', { form: testBooth}, function (error, res, body) {
          //remove the test Booth


          request.post(svr + '/events' , { form: { token: token }  }, function( error, res, body){

            testEvent = JSON.parse(body)[0];
            testBooth['event_id'] = testEvent['_id'];

            done();
          } );



        });
      });
    });
  });
});


describe("Creating new Booth: ", function(){

  describe("/booth/create call", function(){

    it('should throw 401[Unauthorized] on empty query', function (done) {
      request.post(svr + '/booth/create', function (error, res, body) {
        
        res.statusCode.should.equal(401);
        done();
      })
    });

    it('should throw 401[Unauthorized] if name or desc not present', function (done) {
      var data = { form: {name:'', description:''}};
      request.post(svr + '/booth/create', data , function (error, res, body) {
        res.statusCode.should.equal(401);  //Error Status
        done(); 
      });
    });    

    //create user once - should pass
    it('should return 200[OK] and new booth details on sending { name }, { description }, { event_id }  and { other details }', function (done) {
      var data = { form: testBooth};
      request.post(svr + '/booth/create' , data , function (error, res, body) {




        //if(error) console.log(error);

        should.exist(res);
        
        res.statusCode.should.equal(200);

        res.body.should.contain('_id');

        res.body.should.contain('name');
        res.body.should.contain(testBooth.name); 

        res.body.should.contain('description');
        res.body.should.contain(testBooth.description); 

        res.body.should.contain('thumbnail');
        res.body.should.contain(testBooth.thumbnail); 

        if(res.statusCode == 200)   //Store the ID for futher testing below
          testBoothId = JSON.parse(body)["_id"];

        done();      
      });
    });    
  });
});

describe("Reading booth data", function(){
  describe("/booth call", function(){
    it('should return 400[Bad Request] if { _id } not sent', function (done) {
      request.post(svr + '/booth', {form: { token: token} }, function (error, res, body) {
        res.statusCode.should.equal(400);
        done();
      })
    });

    it('should return 200[OK] and booth data for { _id }', function (done) {
      var data = { form:  { _id : testBoothId, token: token } };      
      request.post(svr + '/booth', data , function (error, res, body) {


        var pBody = JSON.parse(body);

/*
        pBody[''].should.equal(testBooth.);
*/
        should.exist(pBody['_id']);
        pBody['event_id'].should.equal(testBooth.event_id);        

        pBody['name'].should.equal(testBooth.name);
        pBody['description'].should.equal(testBooth.description);        
        pBody['thumbnail'].should.equal(testBooth.thumbnail);
        
        pBody['image'].should.equal(testBooth.image);       
        
        pBody['address'].should.equal(testBooth.address);       
        pBody['email'].should.equal(testBooth.email);
        pBody['phone'].should.equal(testBooth.phone);
        pBody['url'].should.equal(testBooth.url);
        pBody['title'].should.equal(testBooth.title);
        
        // pBody['coordinates']['floorplan_id'].should.equal(testBooth.coordinates.floorplan_id);
        // pBody['coordinates']['x'].should.equal(testBooth.coordinates['x']);
        // pBody['coordinates']['y'].should.equal(testBooth.coordinates['y']);

        res.statusCode.should.equal(200);
        done();
      })
    });   
  });
});

describe("Updating booth data", function(){
  describe("/booth/update call", function(){

    it('should return 400[Bad Request] on empty request', function (done) {
      request.post(svr + '/booth/update',  { form : {  token : token } } , function (error, res, body) {
        res.statusCode.should.equal(400);
        done();      
      });
    });



    it('should return 200[OK] & data, on updating via { _id }', function (done) {

      request.post(svr + '/booth/update', {form : { _id: testBoothId, thumbnail: "changed_thumbnail" , token: token } } , function (error, res, body) {
        res.statusCode.should.equal(200);
        JSON.parse(body)["thumbnail"].should.equal('changed_thumbnail');
        done();      
      });

    });    


    it('should return 409[Conflict Occurred] on invalid { _id }', function (done) {

      request.post(svr + '/booth/update', {form : { _id: '00000000000' , token: token } } , function (error, res, body) {
        res.statusCode.should.equal(409);
        done();      
      });

    });

  });
});


describe("Deleting Booths: ", function(){

  describe("/booth/delete call", function(){

    it('should return 400[Bad Request] if { _id }  missing', function (done) {
      request.post(svr + '/booth/delete', { form: { token : token} } , function (error, res, body) {
        res.statusCode.should.equal(400);
        done();
      })
    });

    //delete booths
    it('should return 200[OK] when { _id } is sent', function (done) {

      if(testBoothId){
        request.post(svr + '/booth/delete', { form: { _id: testBoothId, token: token } } , function (error, res, body) {
          res.statusCode.should.equal(200);
          done();
        });        
      }else{
        done( new Error('Test Booth ID[_id] is missing. Unable to test /booth/delete api call') );  
      }
    });
  });
});



describe("Reading Booth Data: ", function(){
  describe("/booth call", function(){
    it('should return 404[Not Found] after booth is deleted', function (done) {

      var data = { form: {_id: testBoothId, token: token} };      
      request.post(svr + '/booth', data , function (error, res, body) {
        res.statusCode.should.equal(404);
        done();
      });
    });
  });
});
