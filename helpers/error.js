module.exports = {




  labels: {  // http://www.restapitutorial.com/httpstatuscodes.html#
    400: "400 - Bad Request",           
    401: "401 - Unauthorized",          //Authentication failed
//  402: "402 - Payment Required",      //Not in use
    403: "403 - Forbidden",             //Don't repeat this request
    404: "404 - Resource Not Found",  
    409: "409 - Conflict Occurred"      //Something didn't agree with something ...  
  },


  format: function( errCode, why, debugData ){
    var error_msg = 
      { error: errCode, 
        message: 
          [
            this.labels[errCode], 
            why
          ].join(': ') ,
        data: debugData
      }

    var new_error_msg = error_msg;
//    console.error("\n"+JSON.stringify(error_msg, null , 4));

    new_error_msg = JSON.stringify(new_error_msg, null , 4);
    new_error_msg = this.prettyjson_unmangle(new_error_msg);

    console.error(new_error_msg);


    return new_error_msg;
  },

  prettyjson_unmangle: function(lump){

    var result = String(lump).replace(/\\u.*?\[.*?m/g, '')

      result = result.replace(/\\n/g, "\n\t\t");


    return result;
  }  









}

